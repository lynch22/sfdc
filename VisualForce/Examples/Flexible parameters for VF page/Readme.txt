Flixible parameters in URL for VF page
If a parameter is omitted, method logic can handle as follows

if(ApexPages.currentPage().getParameters().contains('myParm')) {
	String myParm = ApexPages.currentPage().getParameters().get('myParm');
}else{
// if the parameter does not exist, the get() will return null.
// process as:
	String myParm = ApexPages.currentPage().getParameters().get('myParm');
	if(myParm != null) { logic }
	
NOTE: ApexPages.currentPage().getParameters().get('myParm').METHOD() will fail for missing parm
