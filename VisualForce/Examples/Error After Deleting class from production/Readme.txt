Error following the successful deletion of a class in production.
The cause was a trigger reference of the class.
To reproduce:
1) test class - testtryContact - tests the trigger by inserting a contact
2) trigger code - tryContactAfter - references the class being deleted
3) dummy class - doNothing - referenced class to deleted

Setup everything.
Delete the doNothing class successfully
Run test code, receive error that the trigger fails
